# About the project

**Quest** is an example inspired by [Android Kotlin Fundamentals codelabs 06-07](https://codelabs.developers.google.com/codelabs/kotlin-android-training-room-database/index.html) - Room database, RecyclerView, DiffUtil and data binding. 

Quest is a sample real-life gamification app, which works on honor system. It gives the user 3 random quests (out of the list of hardcoded quests). Upon completing one of these quests, the user has to report it by tapping the "Completed" button. The quest is saved in the journal, together with a random virtual reward the user gets for completing the quest. The list of all rewards can be viewed in the Inventory menu. Currently these rewards are not used for anything.

Quests and journals (completed quest logs) are implemented in the same RecyclerView in order to practice the RecyclerView with Headers. This example is more complicated than the Sleep Tracker from the codelab, as both data sources are LiveData, so they're tracked together through MediatorLiveData. The Inventory screen is for practicing RecyclerView with the Grid layout and Binding Adapters.

The food images are from Food and Kitchenware collection of icons - unfortunately, I don't have a link to its developer, I believe I bought it as a part of some asset bundle.

The apk is here: [quest.apk](apk/quest.apk).
___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)
