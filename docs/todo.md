+ Add option menu (https://codelabs.developers.google.com/codelabs/kotlin-android-training-add-navigation/index.html#8) to the main fragment 

+ Add "About" fragment to menu

+ Add "Reset all" to menu - it goes to a fragment with a warning and 1 button (Reset). On click, it erases all the reports (and rewards - later), reinits the quests, picks 3 active quests again, and goes to the main page.

+ Add "rewards" table: name, description, count. Upload images for rewards. (So the drawable can be derived from the name.)

+ Create a function to select a random reward.

+ Upon completing a quest, update the rewards table with the obtained reward.

+ Add Status (Inventory?) fragment, with the recycler view of the list of rewards as a grid. (Image and count.)

+ Add navigation to go back from all the screens with "Back" arrow.

+ Set title in each fragment accordingly.

+ Improve the appearance:
  - bigger font, padding/margins, visible separation between the entries on the main page
  - comments, bigger font etc on quest detail and report detail
  - maybe include the reward image on report detail
  - change margin dynamically to separate between quests and reports? 

+ Write the About text

+ Improve the appearance of Reset fragment

+ Create a more interesting list of quests

+ Add more rewards

   

