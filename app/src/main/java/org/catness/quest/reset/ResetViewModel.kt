package org.catness.quest.reset

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.catness.quest.database.Journal
import org.catness.quest.database.Quest
import org.catness.quest.database.QuestDatabaseDao

class ResetViewModel(
    dataSource: QuestDatabaseDao
) : ViewModel() {
    private val LOG_TAG: String = this.javaClass.simpleName

    val database = dataSource

    private var _showSnackbarEvent = MutableLiveData<Boolean>()

    val showSnackBarEvent: LiveData<Boolean>
       get() = _showSnackbarEvent


    private val _navigateToQuestsFragment = MutableLiveData<Boolean?>()
    val navigateToQuestsFragment: LiveData<Boolean?>
        get() = _navigateToQuestsFragment

    fun doneNavigating() {
        _navigateToQuestsFragment.value = null
    }

    fun onReset() {
        viewModelScope.launch {
            database.fullResetAllQuests()
            database.clearAllJournals()
            database.resetAllRewards()
            repeat(3) {
                database.activateRandomQuest()
            }
            _showSnackbarEvent.value = true
        }
    }

    fun doneShowingSnackbar() {
       _showSnackbarEvent.value = false
       _navigateToQuestsFragment.value = true

    }



}
