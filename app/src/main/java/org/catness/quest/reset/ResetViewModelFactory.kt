package org.catness.quest.reset

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.catness.quest.database.QuestDatabaseDao

class ResetViewModelFactory(
        private val dataSource: QuestDatabaseDao
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ResetViewModel::class.java)) {
            return ResetViewModel(dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}