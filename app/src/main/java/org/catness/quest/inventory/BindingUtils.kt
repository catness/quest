package org.catness.quest.inventory

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import org.catness.quest.R
import org.catness.quest.database.Reward

@BindingAdapter("rewardImage")
fun ImageView.setRewardImage(item: Reward) {
    // item.name
        var uri: String = "@drawable/" + item.name
        var imageResource: Int = context.resources.getIdentifier(uri, null, context.packageName)
        setImageResource(imageResource)
}

@BindingAdapter("rewardAmount")
fun TextView.setRewardAmount(item: Reward) {
        text = item.amount.toString()
}


