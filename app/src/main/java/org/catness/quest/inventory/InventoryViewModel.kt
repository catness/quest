package org.catness.quest.inventory

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import org.catness.quest.convertLongToDateString
import org.catness.quest.database.Reward
import org.catness.quest.database.QuestDatabaseDao
import org.catness.quest.formatRewards

class InventoryViewModel(
        dataSource: QuestDatabaseDao,
        application: Application
) : ViewModel() {
    val database = dataSource
    val rewards = database.getRewards()

    /**
     * Converted quests to Spanned for displaying.
     */
    val rewardsString = Transformations.map(rewards) { rewards ->
        formatRewards(rewards, application.resources)
    }

}