package org.catness.quest.inventory

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import org.catness.quest.R
import org.catness.quest.database.QuestDatabase
import org.catness.quest.databinding.FragmentInventoryBinding
import androidx.recyclerview.widget.GridLayoutManager
import org.catness.quest.MainActivity

class InventoryFragment : Fragment() {
 
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

      // Get a reference to the binding object and inflate the fragment views.
        val binding: FragmentInventoryBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_inventory, container, false
        )

        val application = requireNotNull(this.activity).application

        // Create an instance of the ViewModel Factory.
        val dataSource = QuestDatabase.getInstance(application).questDatabaseDao
        val viewModelFactory = InventoryViewModelFactory(dataSource, application)

        // Get a reference to the ViewModel associated with this fragment.
        val inventoryViewModel =
            ViewModelProvider(
                this, viewModelFactory
            ).get(InventoryViewModel::class.java)

        // To use the View Model with data binding, you have to explicitly
        // give the binding object a reference to it.
        binding.inventoryViewModel = inventoryViewModel

        val adapter = InventoryAdapter()
        binding.inventoryList.adapter = adapter

        inventoryViewModel.rewards.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.submitList(it)
            }
        })
        binding.setLifecycleOwner(this)

        val manager = GridLayoutManager(activity, 4)
        binding.inventoryList.layoutManager = manager

        (activity as MainActivity).supportActionBar?.title = getString(R.string.inventory)
        return binding.root

    }


}