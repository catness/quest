package org.catness.quest.journaldetail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import org.catness.quest.MainActivity
import org.catness.quest.R
import org.catness.quest.convertLongToDateString
import org.catness.quest.database.Journal
import org.catness.quest.database.QuestDatabaseDao

class JournalDetailViewModel(
    private val journalKey: Long = 0L,
    dataSource: QuestDatabaseDao
) : ViewModel() {
    val database = dataSource

    private val journal: LiveData<Journal> = database.getJournal(journalKey)
    fun getJournal() = journal
    var updatedStr: LiveData<String> = Transformations.map(journal) {
        convertLongToDateString(it.updated)
    }

    init {
        Log.i("JournalDetail","hello init")
    }

}