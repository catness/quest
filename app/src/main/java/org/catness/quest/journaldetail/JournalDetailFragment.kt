package org.catness.quest.journaldetail

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import org.catness.quest.MainActivity
import org.catness.quest.R
import org.catness.quest.database.Journal
import org.catness.quest.database.QuestDatabase
import org.catness.quest.databinding.FragmentJournalDetailBinding

class JournalDetailFragment : Fragment() {
    private val LOG_TAG: String = this.javaClass.simpleName

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
      // Get a reference to the binding object and inflate the fragment views.

        Log.i(LOG_TAG,"Hello onCreateView")

        val binding: FragmentJournalDetailBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_journal_detail, container, false)

        val application = requireNotNull(this.activity).application
        val arguments = arguments?.let { JournalDetailFragmentArgs.fromBundle(it) }

        // Create an instance of the ViewModel Factory.
        val dataSource = QuestDatabase.getInstance(application).questDatabaseDao
        val viewModelFactory = JournalDetailViewModelFactory(arguments!!.journalKey, dataSource)

        // Get a reference to the ViewModel associated with this fragment.
        val journalDetailViewModel =
                ViewModelProvider(
                        this, viewModelFactory).get(JournalDetailViewModel::class.java)

        // To use the View Model with data binding, you have to explicitly
        // give the binding object a reference to it.
        binding.journalDetailViewModel = journalDetailViewModel

        binding.setLifecycleOwner(this)

        // Observer to draw the Reward image.
        // We can't refer to drawables from ViewModel (theoretically it's possible but not recommended)
        // (I actually tried to pass the context to it, and to bind the ImageView with dynamically generated drawable
        // but the app kept crashing)
        // So all the drawable logics has to be in the fragment
        journalDetailViewModel.getJournal().observe(viewLifecycleOwner, Observer<Journal> { it ->
            // but at least we can dynamically generate the drawable name
            var uri: String = "@drawable/${it.reward_name}"
            Log.i("JournalDetail","image: $uri")

            activity?.applicationContext?.let { 
                var imageResource: Int = it.getResources ().getIdentifier(uri, null, it.getPackageName())
                binding.journalRewardImage.setImageResource(imageResource)
            }
        })

        (activity as MainActivity).supportActionBar?.title = getString(R.string.journal)
        return binding.root
    }
}