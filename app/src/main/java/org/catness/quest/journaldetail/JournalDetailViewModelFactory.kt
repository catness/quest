package org.catness.quest.journaldetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.catness.quest.MainActivity
import org.catness.quest.database.QuestDatabaseDao

class JournalDetailViewModelFactory(
        private val journalKey: Long,
        private val dataSource: QuestDatabaseDao
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(JournalDetailViewModel::class.java)) {
            return JournalDetailViewModel(journalKey, dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}