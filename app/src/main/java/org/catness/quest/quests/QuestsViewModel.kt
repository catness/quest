package org.catness.quest.quests

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import org.catness.quest.database.Journal
import org.catness.quest.database.Quest
import org.catness.quest.database.QuestDatabaseDao
import org.catness.quest.formatQuests
import javax.sql.CommonDataSource

class QuestsViewModel(dataSource: QuestDatabaseDao, application: Application) : ViewModel() {
    private val LOG_TAG: String = this.javaClass.simpleName
    val database = dataSource
    val quests = database.getActiveQuests()

    /**
     * Converted quests to Spanned for displaying.
     */
    val questsString = Transformations.map(quests) { quests ->
        formatQuests(quests, application.resources)
    }

    val journals = database.getAllJournals()

    val questsAndJournals : CombinedData? = getAllData()

    // This variable contains the Journal ID when the user clicks on the journal entry.
    private val _navigateToJournalDetail = MutableLiveData<Long>()
    val navigateToJournalDetail
        get() = _navigateToJournalDetail

    fun onJournalDetailNavigated() {
        _navigateToJournalDetail.value = null
    }

    fun onJournalClicked(id: Long) {
        _navigateToJournalDetail.value = id // this is the Journal id in the database!
        Log.i(LOG_TAG, "Clicked journal: " + id.toString())
    }

    // This variable contains the quest ID when the user clicks on the quest entry.
    private val _navigateToQuestDetail = MutableLiveData<Long>()
    val navigateToQuestDetail
        get() = _navigateToQuestDetail

    fun onQuestDetailNavigated() {
        _navigateToQuestDetail.value = null
    }

    fun onQuestClicked(id: Long) {
        _navigateToQuestDetail.value = id // this is the quest id in the database!
        Log.i(LOG_TAG, "Clicked quest: " + id.toString())
    }

    fun getAllData(): CombinedData? {
        var ldQuests = database.getActiveQuests()
        var ldJournals = database.getAllJournals()

        return CombinedData(ldQuests, ldJournals)
    }
}

class CombinedData(
    ldQuest: LiveData<List<Quest>>,
    ldJournal: LiveData<List<Journal>>
) : MediatorLiveData<Pair<List<Quest>, List<Journal>>>() {

    private var listQuest: List<Quest> = emptyList()
    private var listJournal: List<Journal> = emptyList()

    init {
        value = Pair(listQuest, listJournal)

        addSource(ldQuest) {
            if (it != null) listQuest = it
            value = Pair(listQuest, listJournal)
        }

        addSource(ldJournal) {
            if (it != null) listJournal = it
            value = Pair(listQuest, listJournal)
        }
    }
}