package org.catness.quest.quests

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.catness.quest.database.Journal
import org.catness.quest.database.Quest
import org.catness.quest.databinding.ListItemJournalBinding
import org.catness.quest.databinding.ListItemQuestBinding

/**
 * Created by catness on 9/8/20.
 */
private val ITEM_VIEW_TYPE_QUEST = 0
private val ITEM_VIEW_TYPE_JOURNAL = 1


class QuestsAdapter(
    val clickListenerQuest: QuestListener,
    val clickListenerJournal: JournalListener
) :
    ListAdapter<DataItem, ViewHolder>(QuestDiffCallback()) {

    private val adapterScope = CoroutineScope(Dispatchers.Default)

    fun addHeaderAndSubmitList(quests: List<Quest>?, journals: List<Journal>?) {
        adapterScope.launch {
            var items: List<DataItem> = when {
                quests == null -> when {
                    journals == null -> listOf(DataItem.JournalItem(Journal(-1,"No entries yet","Nope")))
                    else -> journals.map { DataItem.JournalItem(it)}
                }
                journals == null -> quests.map {DataItem.QuestItem(it)}
                else -> quests.map { DataItem.QuestItem(it) } + journals.map { DataItem.JournalItem(it) }
            }
            withContext(Dispatchers.Main) {
                submitList(items)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {
            ITEM_VIEW_TYPE_QUEST -> QuestViewHolder.from(parent)
            ITEM_VIEW_TYPE_JOURNAL -> JournalViewHolder.from(parent)
            else -> throw ClassCastException("Unknown viewType ${viewType}")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is DataItem.QuestItem -> ITEM_VIEW_TYPE_QUEST
            is DataItem.JournalItem -> ITEM_VIEW_TYPE_JOURNAL
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (holder) {
            is QuestViewHolder -> {
                val questItem = getItem(position) as DataItem.QuestItem
                holder.bind(questItem.quest, clickListenerQuest)
            }
            is JournalViewHolder -> {
                val journalItem = getItem(position) as DataItem.JournalItem
                holder.bind(journalItem.journal, clickListenerJournal)
            }
        }
    }

    class QuestViewHolder private constructor(val binding: ListItemQuestBinding) :
        ViewHolder(binding.root) {
        fun bind(item: Quest, clickListener: QuestListener) {
            binding.quest = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemQuestBinding.inflate(layoutInflater, parent, false)
                return QuestViewHolder(binding)
            }
        }
    }

    class JournalViewHolder private constructor(val binding: ListItemJournalBinding) :
        ViewHolder(binding.root) {
        fun bind(item: Journal, clickListener: JournalListener) {
            binding.journal = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemJournalBinding.inflate(layoutInflater, parent, false)
                return JournalViewHolder(binding)
            }
        }
    }

}

class QuestListener(val clickListener: (questId: Long) -> Unit) {
    fun onClick(quest: Quest) = clickListener(quest.id)
}

class JournalListener(val clickListener: (journalId: Long) -> Unit) {
    fun onClick(journal: Journal) = clickListener(journal.id)
}

class QuestDiffCallback : DiffUtil.ItemCallback<DataItem>() {
    override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem.id == newItem.id
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem == newItem
    }
}

sealed class DataItem {
    data class JournalItem(val journal: Journal) : DataItem() {
        override val id = journal.id
    }

    data class QuestItem(val quest: Quest) : DataItem() {
        override val id = -quest.id
    }

    abstract val id: Long
}
