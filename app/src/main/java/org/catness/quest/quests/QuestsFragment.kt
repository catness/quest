package org.catness.quest.quests

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import org.catness.quest.R
import org.catness.quest.database.QuestDatabase
import org.catness.quest.databinding.FragmentQuestsBinding
import org.catness.quest.MainActivity

class QuestsFragment : Fragment() {
    private val LOG_TAG: String = this.javaClass.simpleName

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Get a reference to the binding object and inflate the fragment views.
        val binding: FragmentQuestsBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_quests, container, false
        )

        val application = requireNotNull(this.activity).application

        // Create an instance of the ViewModel Factory.
        val dataSource = QuestDatabase.getInstance(application).questDatabaseDao
        val viewModelFactory = QuestsViewModelFactory(dataSource, application)

        // Get a reference to the ViewModel associated with this fragment.
        val questsViewModel =
            ViewModelProvider(
                this, viewModelFactory
            ).get(QuestsViewModel::class.java)

        // To use the View Model with data binding, you have to explicitly
        // give the binding object a reference to it.
        binding.questsViewModel = questsViewModel

        val adapter = QuestsAdapter(
            QuestListener { questId ->
                //Toast.makeText(context, "Quest ${questId}", Toast.LENGTH_SHORT).show()
                questsViewModel.onQuestClicked(questId)
            },
            JournalListener { journalId ->
                //Toast.makeText(context, "Journal ${journalId}", Toast.LENGTH_SHORT).show()
                questsViewModel.onJournalClicked(journalId)
            }
        )
        binding.questList.adapter = adapter

        // adapter.addHeaderAndSubmitList(questsViewModel.quests.value, questsViewModel.journals.value)
        //adapter.addHeaderAndSubmitList(null, null)

        // Specify the current activity as the lifecycle owner of the binding.
        // This is necessary so that the binding can observe LiveData updates.
        binding.setLifecycleOwner(this)
/*

        questsViewModel.quests.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.addHeaderAndSubmitList(it, questsViewModel.journals.value)
            }
        })

        questsViewModel.journals.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.addHeaderAndSubmitList(questsViewModel.quests.value, it)
            }
        })

        */

        questsViewModel.questsAndJournals?.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.addHeaderAndSubmitList(it.first, it.second)
            }
        })

        questsViewModel.navigateToJournalDetail.observe(viewLifecycleOwner, Observer { journalID ->
            journalID?.let {
                Log.i(LOG_TAG, "navigate to journal detail! " + journalID.toString())
                this.findNavController().navigate(
                    QuestsFragmentDirections
                        .actionQuestsFragmentToJournalDetailFragment(journalID)
                )
                questsViewModel.onJournalDetailNavigated()
            }
        })

        questsViewModel.navigateToQuestDetail.observe(viewLifecycleOwner, Observer { questID ->
            questID?.let {
                Log.i(LOG_TAG, "navigate to quest detail! " + questID.toString())
                this.findNavController().navigate(
                    QuestsFragmentDirections
                        .actionQuestsFragmentToQuestDetailFragment(questID)
                )
                questsViewModel.onQuestDetailNavigated()
            }
        })

        setHasOptionsMenu(true)
        (activity as MainActivity).supportActionBar?.title = getString(R.string.app_name)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.options_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item, requireView().findNavController())
                || super.onOptionsItemSelected(item)
    }

}