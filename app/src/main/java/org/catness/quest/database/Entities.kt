package org.catness.quest.database

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "quests",
    indices = arrayOf(
        Index(value = ["active"], name = "active"),
        Index(value = ["completed"], name = "completed"),
        Index(value = ["name"], name = "qname", unique = true)
    )
)
data class Quest(
    var name: String = "",
    var description: String = "",
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L,
    var active: Int = 0,
    var completed: Int = 0
)

@Entity(tableName = "journals")
data class Journal(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L,
    var name: String = "",
    var description: String = "",
    var reward: String = "",
    var reward_name: String = "",
    var updated: Long = System.currentTimeMillis()
)

@Entity(tableName = "rewards",
        indices = arrayOf(
            Index(value = ["name"], name = "rname"),
            Index(value = ["amount"], name = "amount")
            )
    )
data class Reward(
    var name: String = "",
    var description: String = "",
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L,
    var amount: Int = 0
)
