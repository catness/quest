package org.catness.quest.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface QuestDatabaseDao {
    @Insert
    suspend fun insertQuest(quest: Quest)

    @Update
    suspend fun updateQuest(quest: Quest)

    @Query("update quests set active=0, completed=1 where id = :key")
    suspend fun completeQuest(key: Long)

    @Query("select * from quests where active=0 and completed=0 order by random() limit 1")
    suspend fun getRandomQuest(): Quest

    @Query("update quests set active=1 where id in (select id from quests where active=0 and completed =0 order by random() limit 1)")
    suspend fun activateRandomQuest()

    @Query("update quests set active=1 where id in (select id from quests where active=0 order by random() limit 3)")
    fun activateRandomQuestBulk()

    @Query("select count(*) from quests where active=0 and completed=0")
    suspend fun countAvailableQuests(): Long

    @Query("update quests set completed=0 where completed=1")
    suspend fun resetAllQuests()

    @Query("update quests set completed=0, active=0")
    suspend fun fullResetAllQuests()

    @Query("select * from quests where id = :key")
    fun getQuest(key: Long): LiveData<Quest>

    @Query("delete from quests")
    suspend fun clearAllQuests()

    @Insert
    fun insertQuestBulk(data: List<Quest>)

    @Query("select * from quests order by id")
    fun getAllQuests(): LiveData<List<Quest>>

    @Query("select * from quests where active=1 order by id")
    fun getActiveQuests(): LiveData<List<Quest>>

    @Insert
    suspend fun insertJournal(journal: Journal)

    @Update
    suspend fun updateJournal(journal: Journal)

    @Query("select * from journals where id = :key")
    fun getJournal(key: Long): LiveData<Journal>

    @Query("delete from journals")
    suspend fun clearAllJournals()

    @Insert
    fun insertJournalBulk(data: List<Journal>)

    @Query("select * from journals order by id desc")
    fun getAllJournals(): LiveData<List<Journal>>

    @Insert
    fun insertRewardBulk(data: List<Reward>)

    @Query("update rewards set amount=0")
    suspend fun resetAllRewards()

    @Query("update rewards set amount = amount+1 where id = :key")
    suspend fun incrementReward(key: Long)

    @Query("select * from rewards where amount>0 order by name")
    fun getRewards(): LiveData<List<Reward>>

    @Query("select * from rewards order by random() limit 1")
    suspend fun getRandomReward(): Reward

}