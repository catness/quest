package org.catness.quest.database

val questStrings = mapOf(
    "A short walk" to "Walk for 30 min",
    "5 pushups" to "Do 5 pushups",
    "5 situps" to "Do 5 situps",
    "10 push-ups" to "Do 10 pushups",
    "10 sit-ups" to "Do 10 situps",
    "Bike exercise" to "Ride a bike for 30 min",
    "A long walk" to "Walk for 1 hour",
    "Programming exercise" to "Do a programming exercise",
    "Short coding quest" to "Do coding for 30 min",
    "Long coding quest" to "Do coding for 1 hour",
    "Short learning quest" to "Study for 30 min",
    "Long learning quest" to "Study for 1 hour",
    "Learning quest" to "Complete any online lesson",
    "Short writing quest" to "Write for 15 min",
    "Long writing quest" to "Write for 30 min",
    "Writing quest" to "Complete a writing exercise",
    "Blogging quest" to "Write a blog post",
    "Short reading quest" to "Read for 30 min",
    "Long reading quest" to "Read for 1 hour",
    "Water quest" to "Drink a glass of water",
    "Cat quest" to "Pet a cat",
    "Dish quest" to "Wash the dishes",
    "Coffee quest" to "Drink a cup of coffee"
    )

val rewardStrings = mapOf(
    "bacon" to "bacon",
    "slice_of_pizza" to "slice of pizza",
    "hamburger" to "hamburger",
    "hotdog" to "hot dog",
    "chips" to "chips",
    "steak" to "steak",
    "sandwich" to "sandwich",
    "salami" to "salami",
    "chicken" to "chicken",
    "kebab" to "kebab",
    "taco" to "taco",
    "sushi" to "sushi",
    "pizza" to "pizza",
    "fried_eggs" to "fried eggs",
    "fish" to "fish",
    "egg" to "egg",
    "sushi1" to "sushi",
    "sushi2" to "sushi",
    "ramen" to "ramen"
    )



