package org.catness.quest.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import java.util.concurrent.Executors
import org.catness.quest.database.questStrings
import org.catness.quest.database.Quest
import org.catness.quest.database.rewardStrings
import org.catness.quest.database.Reward

@Database(entities = [Quest::class, Journal::class, Reward::class], version = 1, exportSchema = false)
abstract class QuestDatabase : RoomDatabase() {

    /**
     * Connects the database to the DAO.
     */
    abstract val questDatabaseDao: QuestDatabaseDao

    /**
     * Define a companion object, this allows us to add functions on the Database class.
     *
     * For example, clients can call `Database.getInstance(context)` to instantiate
     * a new Database.
     */
    companion object {
        /**
         * INSTANCE will keep a reference to any database returned via getInstance.
         *
         * This will help us avoid repeatedly initializing the database, which is expensive.
         *
         *  The value of a volatile variable will never be cached, and all writes and
         *  reads will be done to and from the main memory. It means that changes made by one
         *  thread to shared data are visible to other threads.
         */
        @Volatile
        private var INSTANCE: QuestDatabase? = null

        /**
         * Helper function to get the database.
         *
         * If a database has already been retrieved, the previous database will be returned.
         * Otherwise, create a new database.
         *
         * This function is threadsafe, and callers should cache the result for multiple database
         * calls to avoid overhead.
         *
         * This is an example of a simple Singleton pattern that takes another Singleton as an
         * argument in Kotlin.
         *
         * To learn more about Singleton read the wikipedia article:
         * https://en.wikipedia.org/wiki/Singleton_pattern
         *
         * @param context The application context Singleton, used to get access to the filesystem.
         */
        fun getInstance(context: Context): QuestDatabase {
            // Multiple threads can ask for the database at the same time, ensure we only initialize
            // it once by using synchronized. Only one thread may enter a synchronized block at a
            // time.
            synchronized(this) {

                // Copy the current value of INSTANCE to a local variable so Kotlin can smart cast.
                // Smart cast is only available to local variables.
                var instance = INSTANCE

                // If instance is `null` make a new database instance.
                if (instance == null) {
                    instance = Room.databaseBuilder(
                            context.applicationContext,
                            QuestDatabase::class.java,
                            "quests_database"
                    )
                    /*
                      An example of prepopulating the database:
                      https://gist.github.com/florina-muntenescu/697e543652b03d3d2a06703f5d6b44b5
                     */
                     .addCallback(object : Callback() {
                            override fun onCreate(db: SupportSQLiteDatabase) {
                                super.onCreate(db)
                                // insert the data on the IO Thread
                                ioThread {
                                    val qdata: List<Quest> = questStrings.map {Quest(it.key,it.value)}
                                    getInstance(context).questDatabaseDao.insertQuestBulk(qdata)
                                    getInstance(context).questDatabaseDao.activateRandomQuestBulk()
                                    val rdata: List<Reward> = rewardStrings.map {Reward(it.key,it.value)}
                                    getInstance(context).questDatabaseDao.insertRewardBulk(rdata)
                                }
                            }
                        })
                        .build()
                    // Assign INSTANCE to the newly created database.
                    INSTANCE = instance
                }

                // Return instance; smart cast to be non-null.
                return instance
            }
        }
    }
}

private val IO_EXECUTOR = Executors.newSingleThreadExecutor()

/**
 * Utility method to run blocks on a dedicated background thread, used for io/database work.
 */
fun ioThread(f : () -> Unit) {
    IO_EXECUTOR.execute(f)
}
