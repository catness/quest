package org.catness.quest.questdetail

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import org.catness.quest.R
import org.catness.quest.database.QuestDatabase
import org.catness.quest.databinding.FragmentQuestDetailBinding
import com.google.android.material.snackbar.Snackbar

class QuestDetailFragment : Fragment() {
    private val LOG_TAG: String = this.javaClass.simpleName

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        // Get a reference to the binding object and inflate the fragment views.

        Log.i(LOG_TAG, "Hello onCreateView")

        val binding: FragmentQuestDetailBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_quest_detail, container, false
        )

        val application = requireNotNull(this.activity).application
        // val arguments = arguments?.let { QuestDetailFragmentArgs.fromBundle(it) }

        // Create an instance of the ViewModel Factory.
        val dataSource = QuestDatabase.getInstance(application).questDatabaseDao
        val viewModelFactory =
            QuestDetailViewModelFactory(requireArguments().getLong("questKey"), dataSource)

        // Get a reference to the ViewModel associated with this fragment.
        val questDetailViewModel =
            ViewModelProvider(
                this, viewModelFactory
            ).get(QuestDetailViewModel::class.java)

        // To use the View Model with data binding, you have to explicitly
        // give the binding object a reference to it.
        binding.questDetailViewModel = questDetailViewModel

        binding.setLifecycleOwner(this)
        // Add an Observer to the state variable for Navigating when a Quality icon is tapped.
        questDetailViewModel.navigateToQuestsFragment.observe(viewLifecycleOwner, Observer {
            if (it == true) { // Observed state is true.
                this.findNavController().navigate(
                    QuestDetailFragmentDirections.actionQuestDetailFragmentToQuestsFragment()
                )
                // Reset state to make sure we only navigate once, even if the device
                // has a configuration change.
                questDetailViewModel.doneNavigating()
            }
        })

        questDetailViewModel.showSnackBarEvent.observe(viewLifecycleOwner, Observer {
           if (it != null) { // Observed state is true.
               Snackbar.make(
                       activity!!.findViewById(android.R.id.content),
                       getString(R.string.reward_message,it),
                       Snackbar.LENGTH_LONG // How long to display the message.
               ).show()
               questDetailViewModel.doneShowingSnackbar()
           }
        })

        return binding.root
    }


}