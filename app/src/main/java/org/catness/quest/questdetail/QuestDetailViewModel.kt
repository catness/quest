package org.catness.quest.questdetail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext
import org.catness.quest.database.Journal
import org.catness.quest.database.Quest
import org.catness.quest.database.Reward
import org.catness.quest.database.QuestDatabaseDao

class QuestDetailViewModel(
    private val questKey: Long = 0L,
    dataSource: QuestDatabaseDao
) : ViewModel() {
    private val LOG_TAG: String = this.javaClass.simpleName

    val database = dataSource

    /** Coroutine variables */

    /**
     * viewModelJob allows us to cancel all coroutines started by this ViewModel.
     */
    private var viewModelJob = Job()

    /**
     * A [CoroutineScope] keeps track of all coroutines started by this ViewModel.
     *
     * Because we pass it [viewModelJob], any coroutine started in this uiScope can be cancelled
     * by calling `viewModelJob.cancel()`
     *
     * By default, all coroutines started in uiScope will launch in [Dispatchers.Main] which is
     * the main thread on Android. This is a sensible default because most coroutines started by
     * a [ViewModel] update the UI after performing some processing.
     */
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val quest: LiveData<Quest> = database.getQuest(questKey)
    fun getQuest() = quest
    private val _navigateToQuestsFragment = MutableLiveData<Boolean?>()
    val navigateToQuestsFragment: LiveData<Boolean?>
        get() = _navigateToQuestsFragment

    fun doneNavigating() {
        _navigateToQuestsFragment.value = null
    }

    private var _showSnackbarEvent = MutableLiveData<String>()
    val showSnackBarEvent: LiveData<String>
       get() = _showSnackbarEvent

    fun onCompleted() {
        viewModelScope.launch {
            val journal = Journal()
            journal.name = quest.value!!.name
            journal.description = quest.value!!.description
            
            val reward = database.getRandomReward()
            journal.reward = reward.description
            journal.reward_name = reward.name
            database.insertJournal(journal)
            database.incrementReward(reward.id)
            database.completeQuest(quest.value!!.id)
            val count: Long = database.countAvailableQuests()
            if (count == 0L) {
                database.resetAllQuests()
                Log.i(LOG_TAG,"Reset all quests")
            }
            database.activateRandomQuest()
            _showSnackbarEvent.value = reward.description
        }
    }

    fun doneShowingSnackbar() {
       _showSnackbarEvent.value = null
       _navigateToQuestsFragment.value = true

    }
     /**
     * Called when the ViewModel is dismantled.
     * At this point, we want to cancel all coroutines;
     * otherwise we end up with processes that have nowhere to return to
     * using memory and resources.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
 

}