package org.catness.quest.questdetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.catness.quest.MainActivity
import org.catness.quest.database.QuestDatabaseDao

class QuestDetailViewModelFactory(
        private val questKey: Long,
        private val dataSource: QuestDatabaseDao
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(QuestDetailViewModel::class.java)) {
            return QuestDetailViewModel(questKey, dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}